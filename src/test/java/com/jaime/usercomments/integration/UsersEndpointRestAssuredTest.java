package com.jaime.usercomments.integration;

import com.jaime.usercomments.entity.User;
import io.restassured.http.ContentType;
import lombok.extern.slf4j.Slf4j;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;

import javax.annotation.PostConstruct;

import static io.restassured.RestAssured.get;
import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.hasKey;
import static org.hamcrest.core.IsEqual.equalTo;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureMockMvc
@DisplayName("Testing Users endpoint")
@Slf4j
public class UsersEndpointRestAssuredTest {

  @LocalServerPort
  private int port;

  private String uri;

  @PostConstruct
  public void init() {
    uri = "http://localhost:" + port;
  }

  @Test
  @DisplayName("No users on start, create a user, and retrieve user created")
  public void getGretingTest() {


    log.info("**** Getting users. Response should be empty list ****");
    get(uri + "/users").
    then()
      .log()
      .body()
      .body("isEmpty()", Matchers.is(true))
      .statusCode(200);


    log.info("**** Creating user with a POST. Response should be return a 201 status ****");
    given()
      .contentType(ContentType.JSON)
      .with().body(new User(null, "John Lennon")).
    when().
      post(uri + "/users/create").
    then()
      .log().body()
      .assertThat().body("$", hasKey("id"))
      .statusCode(201 );


    log.info("**** Getting users. Response should return the user created ****");
    get(uri + "/users").
    then()
      .log().body()
      .assertThat()
      .body("alias[0]", equalTo("John Lennon"))
      .statusCode(200);

  }


}
