package com.jaime.usercomments.service;

import com.jaime.usercomments.entity.User;
import com.jaime.usercomments.repository.UserRepository;
import com.jaime.usercomments.utils.MillionaireAppException;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.AdditionalAnswers.returnsFirstArg;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
@DisplayName("Testing the UserService layer")
public class UserServiceImplTest {

  @InjectMocks
  private UserServiceImpl userService;
  @Mock
  private UserRepository userRepository;

  private final String JOHN_LENNON = "John Lennon";
  private final String PAUL_MCCARTNEY = "Paul McCartney";

  @Nested
  @DisplayName("Testing createUser method")
  class CreateUserTest {

    @Test
    @DisplayName("GIVEN User, with id null, and alias NOT null, WHEN invoking createUser method, THEN user is created")
    public void createUserHappyPath() throws Exception {
      //given
      given(userRepository.save((any())))
        .will(returnsFirstArg());
      User userToCreate = new User(null, JOHN_LENNON);

      //when
      User userCreated = userService.createUser(userToCreate);

      //then
      assertEquals(userToCreate, userCreated);
      verify(userRepository, times(1)).save(userToCreate);
      verifyNoMoreInteractions(userRepository);
    }

    @Test
    @DisplayName("GIVEN User, with id NOT null, and alias NOT null, WHEN invoking createUser method, THEN MillionaireAppException is thrown with code USER_ID_MUST_BE_NULL, AND repo is not called")
    public void createUserThrowsExcepionIfIdIsNotNull() {
      //given
      User userToCreate = new User(1L, JOHN_LENNON);

      //when + then
      MillionaireAppException exceptionThrown = assertThrows(MillionaireAppException.class,
        () -> userService.createUser(userToCreate));

      //then
      assertEquals(HttpStatus.BAD_REQUEST, exceptionThrown.getStatus());
      verifyNoInteractions(userRepository);
    }

  }

  @Nested
  @DisplayName("Testing updateUser method")
  class UpdateUserTest {

    @Test
    @DisplayName("GIVEN User, with id NOT null, and alias NOT null, WHEN invoking updateUser method, THEN user is updated")
    public void updateUserHappyPath() throws MillionaireAppException {
      //given
      User userWithNewAliasToUpdate = new User(1L, JOHN_LENNON);
      given(userRepository.findById(1L))
        .willReturn(Optional.of(userWithNewAliasToUpdate));
      given(userRepository.save(any()))
        .will(returnsFirstArg());

      //when
      User userUpdated = userService.updateUser(userWithNewAliasToUpdate);

      //then
      assertEquals(userWithNewAliasToUpdate, userUpdated);
      verify(userRepository, times(1)).save(userWithNewAliasToUpdate);
      verifyNoMoreInteractions(userRepository);
    }

    @Test
    @DisplayName("GIVEN User, if user is not found by id on repository, WHEN calling updateUser method, THEN MillionaireAppException is thrown with code USER_DOES_NOT_EXIST")
    public void updateUserThrowsExceptionIfUserDoesNotExist() {
      //given
      User userWithNewAliasToUpdate = new User(1L, JOHN_LENNON);
      given(userRepository.findById(anyLong()))
        .willReturn(Optional.empty());

      //when + then
      MillionaireAppException exceptionThrown = assertThrows(MillionaireAppException.class,
        () -> userService.updateUser(userWithNewAliasToUpdate));

      //then
      assertEquals(HttpStatus.BAD_REQUEST, exceptionThrown.getStatus());
      verify(userRepository, times(1)).findById(1L);
      verifyNoMoreInteractions(userRepository);
    }

  }

  @Nested
  @DisplayName("Testing getUser method")
  class GetUserTest {

    @Test
    @DisplayName("GIVEN userId, and this userId is found on repository, WHEN calling getUser of the service, THEN the user will be returned")
    public void getUserHappyPath() throws MillionaireAppException {
      //given
      final Long USER_ID = 1L;
      User userFoundOnRepo = new User(USER_ID, JOHN_LENNON);
      given(userRepository.findById(anyLong())).willReturn(Optional.of(userFoundOnRepo));

      //when
      User userGetted = userService.getUser(USER_ID);

      //then
      assertEquals(userFoundOnRepo, userGetted);
      verify(userRepository, times(1)).findById(USER_ID);
      verifyNoMoreInteractions(userRepository);
    }

    @Test
    @DisplayName("GIVEN userId, and this userId is NOT found on repository, WHEN calling getUser of the service, THEN MillionaireAppException is thrown with code USER_DOES_NOT_EXIST")
    public void getUserExceptionThrownWhenUserNotFound() {
      //given
      final Long USER_ID = 1L;
      given(userRepository.findById(anyLong()))
        .willReturn(Optional.empty());

      //when + then
      MillionaireAppException exceptionThrown = assertThrows(MillionaireAppException.class,
        () -> userService.getUser(USER_ID));

      //then
      assertEquals(HttpStatus.BAD_REQUEST, exceptionThrown.getStatus());
      verify(userRepository, times(1)).findById(USER_ID);
      verifyNoMoreInteractions(userRepository);
    }

  }

  @Nested
  @DisplayName("Testing getUsers method")
  class GetUsersTest {

    @Test
    @DisplayName("GIVEN users are present on repository, WHEN calling getUsers of the service, THEN users will be returned")
    public void getUsersHappyPath() {
      //given
      User userJohn =
        new User(1L, JOHN_LENNON);
      User userPaul =
        new User(2L, PAUL_MCCARTNEY);
      List<User> usersFound =
        Arrays.asList(userJohn, userPaul);
      given(userRepository.findAll())
        .willReturn(usersFound);

      //when
      List<User> usersGetted = userService.getUsers();

      //then
      assertEquals(usersFound, usersGetted);
      verify(userRepository, times(1)).findAll();
      verifyNoMoreInteractions(userRepository);
    }

    @Test
    @DisplayName("GIVEN user repository is empty, WHEN calling getUsers of the service, THEN empty list of users will be returned")
    public void getUsersEmptyList() {
      //given
      List<User> usersFound =
        new ArrayList<>();
      given(userRepository.findAll())
        .willReturn(usersFound);

      //when
      List<User> usersGetted = userService.getUsers();

      //then
      assertEquals(usersFound, usersGetted);
      assertEquals(0, usersGetted.size());
      verify(userRepository, times(1)).findAll();
      verifyNoMoreInteractions(userRepository);
    }

  }

  @Nested
  @DisplayName("Testing deleteUser method")
  class DeleteUserTest {

    @Test
    @DisplayName("GIVEN user is present on repository, WHEN calling deleteUser of the service, THEN delete method of the repo will be invoked")
    public void deleteUserHappyPath() throws MillionaireAppException {
      //given
      final Long USER_ID = 1L;

      User userJohn = new User(USER_ID, JOHN_LENNON);
      given(userRepository.findById(anyLong()))
        .willReturn(Optional.of(userJohn));

      //when
      userService.deleteUser(USER_ID);

      //then
      verify(userRepository, times(1)).findById(USER_ID);
      verify(userRepository, times(1)).deleteById(USER_ID);
      verifyNoMoreInteractions(userRepository);
    }

    @Test
    @DisplayName("GIVEN user_id not present on repository, WHEN calling deleteUser of the service, THEN exception will be thrown with code USER_DOES_NOT_EXIST")
    public void deleteUserThrowExepctionIfNotExist() {
      //given
      final Long USER_ID = 1L;
      given(userRepository.findById(anyLong()))
        .willReturn(Optional.empty());

      //when + then
      MillionaireAppException exceptionThrown = assertThrows(MillionaireAppException.class,
        () -> userService.deleteUser(USER_ID));

      //then
      assertEquals(HttpStatus.BAD_REQUEST, exceptionThrown.getStatus());
      verify(userRepository, times(1)).findById(USER_ID);
      verifyNoMoreInteractions(userRepository);
    }

  }

}