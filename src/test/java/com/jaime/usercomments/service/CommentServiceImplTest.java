package com.jaime.usercomments.service;

import com.jaime.usercomments.entity.Comment;
import com.jaime.usercomments.entity.User;
import com.jaime.usercomments.repository.CommentRepository;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.mockito.AdditionalAnswers.returnsFirstArg;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.verify;

@ExtendWith(MockitoExtension.class)
@DisplayName("Testing the UserService layer")
public class CommentServiceImplTest {


  @InjectMocks
  private CommentServiceImpl commentService;
  @Mock
  private CommentRepository commentRepository;

  @Nested
  @DisplayName("Testing createUser method")
  class CreateCommentTest {

    @Test
//    @DisplayName("Given user exist")
    public void createCommentHappyPath() {
      //given
      User user = new User(1L, "John Lennon");
      Comment comment = Comment.builder().id(null)
                                         .content("All you need is love")
                                         .user(user).build();


      given(commentRepository.save((any())))
        .will(returnsFirstArg());

//      //when
//      Comment commentCreated = commentService.createComment(comment);
//
//      //then
//      assertEquals(comment, commentCreated);
//      verify(commentRepository, times(1)).save(comment);
//      verifyNoMoreInteractions(commentRepository);
    }

  }

  @Test
  void updateCommennt() {}

  @Test
  void getComment() {}

  @Test
  void getCommentsByUserId() {}

  @Test
  void getCommentsByAlias() {}

  @Test
  void deleteComment() {}
}