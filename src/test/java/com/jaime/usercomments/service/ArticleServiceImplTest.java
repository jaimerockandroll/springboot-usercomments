package com.jaime.usercomments.service;

import com.jaime.usercomments.dto.ArticleDto;
import com.jaime.usercomments.entity.ArticleEntity;
import com.jaime.usercomments.repository.ArticleRepository;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@ExtendWith(MockitoExtension.class)
@DisplayName("Testing the ArticleService layer")
public class ArticleServiceImplTest {

    @Mock
    private ArticleRepository articleRepository;
    @InjectMocks
    private ArticleServiceImpl articleService;

    @Test
    public void createArticleTest() {

        // given an article, with id null
        ArticleDto article = new ArticleDto();
        // given, mocked repo
        given(articleRepository.save(any())).willReturn(ArticleEntity.builder()
                                                            .id(1L).build());

        // when i call to createArticle
        article = articleService.createArticle(article);

        // then, save method of repository is called
        verify(articleRepository, times(1)).save(any(ArticleEntity.class));
        // then article is returned with id>0
        assertTrue(article.getId() >0 );


    }

}
