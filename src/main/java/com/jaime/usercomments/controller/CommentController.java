package com.jaime.usercomments.controller;

import com.jaime.usercomments.entity.Comment;
import com.jaime.usercomments.dto.CreateCommentDto;
import com.jaime.usercomments.service.CommentService;
import org.slf4j.Logger;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("comments/")
public class CommentController {

  private static final Logger log = org.slf4j.LoggerFactory.getLogger(CommentController.class);
  private final CommentService commentService;

  public CommentController(CommentService commentService) {
    this.commentService = commentService;
  }

  @PostMapping(path="create", consumes = {MediaType.APPLICATION_JSON_VALUE})
  @ResponseStatus(HttpStatus.CREATED)
  public Comment createComment(@RequestBody CreateCommentDto createCommentDto) {
    return commentService.createComment(createCommentDto);
  }

  @PostMapping(path="update/{commentId}", consumes = {MediaType.TEXT_PLAIN_VALUE})
  public Comment updateComment(@RequestBody String content, @PathVariable("commentId") Long commentId) {
    return commentService.updateComment(content, commentId);
  }

  @GetMapping("user/{userId}")
  public List<Comment> getComments(@PathVariable("userId") Long userId) {
    return commentService.getCommentsByUserId(userId);
  }

}
