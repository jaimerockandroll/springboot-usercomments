package com.jaime.usercomments.controller;

import com.jaime.usercomments.entity.Comment;
import com.jaime.usercomments.entity.User;
import com.jaime.usercomments.dto.UserAndComments;
import com.jaime.usercomments.service.CommentService;
import com.jaime.usercomments.service.UserService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@Slf4j
@RequestMapping("users")
public class UserController {

  private final UserService userService;
  private final CommentService commentService;

  @PostMapping(path="create", consumes = {MediaType.APPLICATION_JSON_VALUE})
  @ResponseStatus(HttpStatus.CREATED)
  public User createUser(@RequestBody User user) {
    log.info("createUser called with: " + user);
    return userService.createUser(user);
  }

  @PostMapping(path="update", consumes = {MediaType.APPLICATION_JSON_VALUE})
  @ResponseStatus(HttpStatus.OK)
  public User updateUser(@RequestBody User user) {
    return userService.updateUser(user);
  }

  @GetMapping("{userId}")
  public User getUser(@PathVariable("userId") Long userId) {
    return userService.getUser(userId);
  }

  @GetMapping
  public List<User> getUsers() throws Exception {
    return userService.getUsers();
  }

  @GetMapping("{userId}/comments")
  public UserAndComments getUserAndComments(@PathVariable("userId") Long userId) {
    User user = userService.getUser(userId);
    List<Comment> comments = commentService.getCommentsByUserId(userId);
    return new UserAndComments(user, comments);
  }

}
