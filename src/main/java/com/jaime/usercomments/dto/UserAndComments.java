package com.jaime.usercomments.dto;

import com.jaime.usercomments.entity.Comment;
import com.jaime.usercomments.entity.User;
import lombok.Data;

import java.util.List;

@Data
public class UserAndComments {

  public UserAndComments(User user, List<Comment> comments) {
    this.user = user;
    this.comments = comments;
  }

  private User user;
  private List<Comment> comments;

}
