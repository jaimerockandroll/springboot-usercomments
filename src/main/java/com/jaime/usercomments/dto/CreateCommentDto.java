package com.jaime.usercomments.dto;

import lombok.Data;

@Data
public class CreateCommentDto {

  private Long userId;

  private String commentContent;

}
