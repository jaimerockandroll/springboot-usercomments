package com.jaime.usercomments.entity;

import lombok.*;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.sql.Timestamp;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Comment {

  @Id
  @GeneratedValue
  private Long id;

  @Column(nullable = false)
  private String content;

  @JoinColumn(name = "USER_ID", referencedColumnName = "ID", nullable = false)
  @ManyToOne
  private User user;

  @CreationTimestamp
  @Column(name = "created_at", updatable = false)
  @EqualsAndHashCode.Exclude
  @Getter(AccessLevel.NONE)
  @Setter(AccessLevel.NONE)
  private Timestamp createdAt;

  @UpdateTimestamp
  @Column(name = "updated_at")
  @EqualsAndHashCode.Exclude
  @Getter(AccessLevel.NONE)
  @Setter(AccessLevel.NONE)
  private Timestamp updatedAt;

}
