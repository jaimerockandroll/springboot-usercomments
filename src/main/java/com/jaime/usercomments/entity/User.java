package com.jaime.usercomments.entity;

import lombok.*;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.sql.Timestamp;

@Entity
@Data
public class User {

  public User() {}

  public User(Long id, String alias) {
    this.id = id;
    this.alias = alias;
  }

  @Id
  @GeneratedValue
  private Long id;

  @Column(unique = true, nullable = false)
  private String alias;


//  @OneToMany(fetch = FetchType.LAZY, mappedBy = "user", cascade={CascadeType.ALL})
//  private List<Comment> comments;

  @CreationTimestamp
  @Column(name = "created_at", updatable = false)
  @EqualsAndHashCode.Exclude
  @Getter(AccessLevel.NONE)
  @Setter(AccessLevel.NONE)
  private Timestamp createdAt;

  @UpdateTimestamp
  @Column(name = "updated_at")
  @EqualsAndHashCode.Exclude
  @Getter(AccessLevel.NONE)
  @Setter(AccessLevel.NONE)
  private Timestamp updatedAt;

}

