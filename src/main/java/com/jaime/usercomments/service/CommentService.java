package com.jaime.usercomments.service;

import com.jaime.usercomments.entity.Comment;
import com.jaime.usercomments.dto.CreateCommentDto;

import java.util.List;

public interface CommentService {

  Comment createComment(CreateCommentDto comment);

  Comment updateComment(String content, Long commentId);

  Comment getComment(Long commentId);

  List<Comment> getCommentsByUserId(Long userId);

  List<Comment> getCommentsByAlias(String userAlias);

  void deleteComment(Long commentId);

}
