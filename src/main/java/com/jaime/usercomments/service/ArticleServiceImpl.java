package com.jaime.usercomments.service;

import com.jaime.usercomments.dto.ArticleDto;
import com.jaime.usercomments.entity.ArticleEntity;
import com.jaime.usercomments.repository.ArticleRepository;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class ArticleServiceImpl implements ArticleService{

    private final ArticleRepository articleRepository;

    @Override
    public ArticleDto createArticle(ArticleDto articleDto) {
        ArticleEntity articleEntity = convertDtoToEntity(articleDto);

        articleEntity = articleRepository.save(articleEntity);

        return convertEntityToDto(articleEntity);
    }

    private ArticleEntity convertDtoToEntity(ArticleDto articleDto) {
        return ArticleEntity.builder()
                .id(articleDto.getId())
                .build();

    }

    private ArticleDto convertEntityToDto(ArticleEntity articleEntity) {
        return ArticleDto.builder()
                .id(articleEntity.getId())
                .build();
    }

}
