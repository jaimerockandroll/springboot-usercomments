package com.jaime.usercomments.service;

import com.jaime.usercomments.entity.Comment;
import com.jaime.usercomments.entity.User;
import com.jaime.usercomments.dto.CreateCommentDto;
import com.jaime.usercomments.repository.CommentRepository;
import com.jaime.usercomments.repository.UserRepository;
import com.jaime.usercomments.utils.MillionaireAppException;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class CommentServiceImpl implements CommentService{


  private final CommentRepository commentRepository;
  private final UserRepository userRepository;

  @Override
  public Comment createComment(CreateCommentDto createCommentDto) {

    User user = userRepository.findById(createCommentDto.getUserId())
      .orElseThrow(MillionaireAppException::USER_DOES_NOT_EXIST);

    Comment comment = Comment.builder().id(null)
                                       .content(createCommentDto.getCommentContent())
                                       .user(user)
                                       .build();

    return commentRepository.save(comment);
  }

  @Override
  public Comment updateComment(String content, Long commentId) {

    Comment comment = getComment(commentId);

    comment.setContent(content);

    return commentRepository.save(comment);
  }

  @Override
  public Comment getComment(Long commentId) {
    return commentRepository.findById(commentId)
      .orElseThrow(MillionaireAppException::COMMENT_DOES_NOT_EXIST);
  }

  @Override
  public List<Comment> getCommentsByUserId(Long userId) {
    if (userId == null) throw MillionaireAppException.USER_ID_MUST_BE_NOT_NULL();
    return commentRepository.findByUserId(userId);
  }

  @Override
  public List<Comment> getCommentsByAlias(String userAlias) {
    return commentRepository.findByUserAlias(userAlias);
  }

  @Override
  public void deleteComment(Long commentId) {
    validateCommentExist(commentId);
    commentRepository.deleteById(commentId);
  }

  private Comment validateCommentExist(Long commentId) {
    if (commentId == null) throw MillionaireAppException.COMMENT_ID_MUST_BE_NOT_NULL();
    return getComment(commentId);
  }

}
