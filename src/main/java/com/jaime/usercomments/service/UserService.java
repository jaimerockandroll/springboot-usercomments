package com.jaime.usercomments.service;

import com.jaime.usercomments.entity.User;

import java.util.List;

public interface UserService {

  User createUser(User user);

  User updateUser(User user);

  User getUser(Long userId);

  List<User> getUsers();

  void deleteUser(Long userId);

}
