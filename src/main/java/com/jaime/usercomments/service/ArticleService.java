package com.jaime.usercomments.service;

import com.jaime.usercomments.dto.ArticleDto;

public interface ArticleService {

    ArticleDto createArticle(ArticleDto articleDto);
}
