package com.jaime.usercomments.service;

import com.jaime.usercomments.utils.MillionaireAppException;
import com.jaime.usercomments.entity.User;
import com.jaime.usercomments.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
@Slf4j
public class UserServiceImpl implements UserService{

  private final UserRepository userRepository;

  @Override
  public User createUser(User user) {
    if (user.getId()!=null) throw MillionaireAppException.USER_ID_MUST_BE_NULL();
    return userRepository.save(user);
  }

  @Override
  public User updateUser(User user) {
    validateUserExist(user.getId());
    return userRepository.save(user);
  }

  @Override
  public User getUser(Long userId)  {
    if (userId == null) throw MillionaireAppException.USER_ID_MUST_BE_NOT_NULL();
    return userRepository.findById(userId)
      .orElseThrow(MillionaireAppException::USER_DOES_NOT_EXIST);
  }

  @Override
  public List<User> getUsers() {
    return (List<User>) userRepository.findAll();
  }

  @Override
  public void deleteUser(Long userId) {
    validateUserExist(userId);
    userRepository.deleteById(userId);
  }

  private void validateUserExist(Long userId) {
    getUser(userId);
  }



}
