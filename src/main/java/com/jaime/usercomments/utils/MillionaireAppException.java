package com.jaime.usercomments.utils;

import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;

import static org.springframework.http.HttpStatus.BAD_REQUEST;

@Slf4j
public class MillionaireAppException extends ResponseStatusException {

  private MillionaireAppException(HttpStatus httpStatus, String message) {
    super(httpStatus, message);
//    log.warn("MillionaireAppException thrown. Message: " + message);
  }

  public static MillionaireAppException USER_DOES_NOT_EXIST() {
    return new MillionaireAppException(BAD_REQUEST, "User does not exist");
  }
  public static MillionaireAppException USER_ID_MUST_BE_NULL() {
    return new MillionaireAppException(BAD_REQUEST, "User id must be null");
  }
  public static MillionaireAppException USER_ID_MUST_BE_NOT_NULL() {
    return new MillionaireAppException(BAD_REQUEST, "User id must be NOT null");
  }
  public static MillionaireAppException COMMENT_DOES_NOT_EXIST() {
    return new MillionaireAppException(BAD_REQUEST, "Comment does not exist");
  }
  public static MillionaireAppException COMMENT_ID_MUST_BE_NOT_NULL() {
    return new MillionaireAppException(BAD_REQUEST, "Comment id must be NOT null");
  }

}


