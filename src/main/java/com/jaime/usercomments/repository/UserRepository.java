package com.jaime.usercomments.repository;

import com.jaime.usercomments.entity.User;
import org.springframework.data.repository.CrudRepository;

public interface UserRepository extends CrudRepository<User, Long> {
}
